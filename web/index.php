<?php
use framzod\core\Framzod;

session_start();

if (file_exists('../config/config.php')) {
    include_once '../config/config.php';
    include_once CORE_PATH.'/framzod.php';
    $framzod = new Framzod();
    $framzod->main();
} else {
    include '../core/install.php';
}
