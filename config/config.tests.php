<?php
/*
*       SITE_NAME can be user for prefixed title pages, descriptions, ...
*/
define("SITE_NAME", "Framzod");
define("SITE_URL", "https://framzod.codazzi.fr");
/*
*               SITEVERSION is the config of your site.
*
*               dev : Show error in HTML + reset OPcache (for all sites)
*               test : No error in HTML + reset OPcache (for all sites)
*               prod : No error
*/
define("SITEVERSION", 'dev'); // dev test prod

/*      @TODO
 *       Choose addons to load at eatch requests.
 *       For better performance choose only the addons who are used.
 *       For load an addon for a specific request, you can use the function "@TODO" for load it manualy
 */
$GLOBALS['ADDONS_ENABLE'] = [
    'apy',
    'authentication',
    'crypto',
    'lang',
    'orm',
    'html'
];

$GLOBALS['ROUTER_CLASS'] = [
    'Api'
];

/*
*               Default file, class and method to load
*               This class will be load when 404 error, or no param in the URL
*
*/
define('ERROR404_CLASS', "IndexController");
define('ERROR404_CLASSFILE', "Index");
define('ERROR404_METHOD', "notfound");


/*
*               Here is the various paths,
*               you are not suppose to modify them
*/
define("ROOT_PATH", dirname(__FILE__).'/..');
define("ENTITY_PATH", ROOT_PATH.'/src/entities');
define("ADDON_PATH", ROOT_PATH.'/addons');
define("SOURCES_PATH", ROOT_PATH.'/src');
define("CORE_PATH", ROOT_PATH.'/core');
define("CONTENT", ROOT_PATH.'/content');
define("RESOURCES_PATH", ROOT_PATH.'/res');
define("LOCALE_PATH", RESOURCES_PATH.'/locales');

/**
 * Addons defines
 * */
define("ORM_TYPE", 'doctrine');
define("ORM_DRIVER", 'pdo_mysql');
define("ORM_DB_USER", 'unit_test_framzod');
define("ORM_DB_PASSWORD", 'GxDBLSYwkmksSq1D');
define("ORM_DB_NAME", 'unit_test_framzod');
define("ORM_DB_HOST", '192.168.2.20');
define("ORM_DB_CHARSET", 'utf8');
define("ORM_DB_REDIS_HOST", '192.168.2.43');
define("ORM_DB_REDIS_PORT", 6379);
define("ORM_DB_REDIS_NAMESPACE", 'framzod_api_tests');

/**
 * AUTO INCLUDES
 * */
$GLOBALS['AUTO_INCLUDE'] = [
    SOURCES_PATH . '/controllers/DefaultController.php',
    SOURCES_PATH . '/controllers/api/DefaultController.php',
];

/*
*       Your's Defines
*
*/
define("DEFAULT_LANG", 'fr_FR');
