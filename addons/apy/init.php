<?php

namespace framzod\addons;

use framzod\core\Framaddons;
use framzod\core\FzRequest;

/*
*
*/

/**
 * Class Apy
 * @package framzod\addons
 */
class Apy extends Framaddons
{
    /**
     * @var string
     */
    public $name = 'Apicheck';
    /**
     * @var string
     */
    public $author = 'Stéphane Codazzi';
    /**
     * @var string
     */
    public $version = '1.0';
    /**
     * @var string
     */
    public $website = 'https://codazzi.fr';
    /**
     * @var string
     */
    public $description = "";
    /**
     * @var string
     */
    public $licence = 'MIT';

    /**
     * Apy constructor.
     */
    public function __construct()
    {
    }

    /**
     * conditions:
     *  - method : string (GET/POST)
     *  - external[class, method] : Function for authentication or null
     *  - require : array string required data
     * */
    public function isValid($obj, $conditions)
    {
        /* Check http method */
        if (!empty($conditions['method']) && $conditions['method'] !== $obj->request->method) {
            $obj->error = 1;
            return false;
        }

        if (!empty($conditions['require'])) {
            foreach ($conditions['require'] as $field) {
                if (empty($obj->data[$field])) {
                    $obj->error = 2;
                    $obj->result['missing_require_field'] = $field;
                    return false;
                }
            }
        }

        if (!empty($conditions['external'])) {
            $external = $conditions['external'];
            if (is_array($external)) {
                $external[0]->{$external[1]}();
            } else {
                $external();
            }
        }

        return true;
    }

    /**
     * Simulate a API JSON call faster than a real call (no network traffic)
     * Example : Apy::call($this, 'users', 'create', 'POST', array());
     * @param mixed $parent : parent class
     * @param mixed $api_class : class to call
     * @param mixed $api_method : function to call
     * @param string $http_method Http method (POST,GET,DELETE,UPDATE)
     * @param array $data formated array
     * @return mixed API function result, in json format
     */
    public static function call($parent, $api_class, $api_method, $http_method, $data)
    {
        if (!class_exists('Api' . ucfirst($api_class))) {
            $class_file = SOURCES_PATH . '/controllers/api/' . ucfirst($api_class) . 'Controller.php';
            include_once $class_file;
        }
        $api_class_name = 'framzod\controllers\Api' . ucfirst($api_class) . 'Controller';
        $api_class = new $api_class_name();

        /* INIT */
        $api_class->request = new FzRequest();
        $api_class->request->method = $http_method;
        $api_class->data = $data;
        $api_class->addons = $parent->addons;
        $api_class->em = $parent->em;
        $api_class->init();

        /* CALL */
        $api_class->{$api_method}();
        $result = $api_class->result;
        $result['error'] = $api_class->error;
        return $result;
    }
}
