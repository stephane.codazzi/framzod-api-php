<?php

namespace framzod\addons;

use framzod\core\Framaddons;

/*
*       Twig requirement change to vendors lib (via composer)
*       Defines to use : (lines need to be copied in config.php)
*
*/

/**
 * Class Html
 * @package framzod\addons
 */
class Html extends Framaddons
{
    /**
     * @var string
     */
    public $name = 'Html';
    /**
     * @var string
     */
    public $author = 'CodazziS';
    /**
     * @var string
     */
    public $version = '2.0';
    /**
     * @var string
     */
    public $website = '-';
    /**
     * @var string
     */
    public $description = "The html class use the Twig engine.";
    /**
     * @var string
     */
    public $licence = 'Cf twig Licence';

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * Html constructor.
     */
    public function __construct()
    {
        include_once __DIR__ . '/../../vendor/autoload.php';

        $loader = new \Twig_Loader_Filesystem(SOURCES_PATH . '/views');
        $this->twig = new \Twig_Environment($loader);
        $this->twig->addExtension(new \Twig_Extensions_Extension_I18n());
    }

    /**
     *
     */
    public function __destruct()
    {
    }

    /**
     * @param $fzclass
     */
    public function render($fzclass)
    {
        echo $this->twig->render($fzclass->view, $fzclass->result);
    }
}
