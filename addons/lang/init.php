<?php

namespace framzod\addons;

use framzod\core\Framaddons;

/*
*
*/

/**
 * Class Lang
 * @package framzod\addons
 */
class Lang extends Framaddons
{
    /**
     * @var string
     */
    public $name = 'Lang';
    /**
     * @var string
     */
    public $author = 'Stéphane Codazzi';
    /**
     * @var string
     */
    public $version = '1.0';
    /**
     * @var string
     */
    public $website = 'https://codazzi.fr';
    /**
     * @var string
     */
    public $description = "Add internationnalisation with getText files";
    /**
     * @var string
     */
    public $licence = 'MIT';

    /**
     * @var
     */
    private $lang;

    /**
     * Lang constructor.
     */
    public function __construct()
    {
        $this->lang = DEFAULT_LANG;

        /* Get default lang */
        if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $this->lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5);
        }
        /* Look if lang is forced */
        if (isset($_GET['lang']) && $_GET['lang'] !== null) {
            $this->lang = $_GET['lang'];
        }

        putenv('LC_ALL=' . $this->lang);
        setlocale(LC_ALL, $this->lang);
        bindtextdomain("site", ROOT_PATH . "config/locale");
        textdomain("site");

        setcookie("lang", $this->lang, time() + 86400, '/');
    }

    /**
     * @return mixed
     */
    public function getLang()
    {
        return $this->lang;
    }
}
