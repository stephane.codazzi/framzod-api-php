<?php

namespace framzod\addons;

use framzod\core\Framaddons;

/*
*
*/

/**
 * Class Crypto
 * @package framzod\addons
 */
class Crypto extends Framaddons
{
    /**
     * @var string
     */
    public $name = 'Crypto';
    /**
     * @var string
     */
    public $author = 'Stéphane Codazzi';
    /**
     * @var string
     */
    public $version = '1.0';
    /**
     * @var string
     */
    public $website = 'https://codazzi.fr';
    /**
     * @var string
     */
    public $description = "Hash, encrypt and decrypt methods";
    /**
     * @var string
     */
    public $licence = 'MIT';

    /**
     * Crypto constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $str
     * @return string
     */
    public static function hash1($str)
    {
        return hash("sha512", $str, false);
    }

    /**
     * @param $str
     * @return string
     */
    public static function hash2($str)
    {
        return hash("sha256", $str, false);
    }

    /**
     * @param $data
     * @param $key
     * @return string
     */
    public static function encrypt($data, $key)
    {
        if ($data != null && $data != '') {
            $td = mcrypt_module_open(MCRYPT_TRIPLEDES, '', 'nofb', ''); // charge le chiffrement
            $ks = mcrypt_enc_get_key_size($td); //détermine la taille de la clé
            $key = substr(md5($key), 0, $ks); //creation de la cle
            $key2 = substr(md5($key), 0, 8); //creation de la cle 2
            mcrypt_generic_init($td, $key, $key2); // initialisation
            $encrypted = mcrypt_generic($td, $data); //Chiffrement
            mcrypt_generic_deinit($td); // Liberation du chiffrement
            mcrypt_module_close($td);
            return base64_encode($encrypted);
        }
        return '';
    }

    /**
     * @param $data
     * @param $key
     * @return string
     */
    public static function decrypt($data, $key)
    {
        if ($data != null && $data != '') {
            $data = base64_decode($data);
            $td = mcrypt_module_open(MCRYPT_TRIPLEDES, '', 'nofb', ''); // charge le chiffrement
            $ks = mcrypt_enc_get_key_size($td); //détermine la taille de la clé
            $key = substr(md5($key), 0, $ks); //creation de la cle
            $key2 = substr(md5($key), 0, 8); //creation de la cle 2
            mcrypt_generic_init($td, $key, $key2); // Initialisation pour le déchiffrement
            $decrypted = mdecrypt_generic($td, $data); // décryptage
            mcrypt_generic_deinit($td);//liberation
            mcrypt_module_close($td);
            return $decrypted;
        }
    }

    /**
     * @param $str
     * @return bool|string
     */
    public static function formatPhoneNumber($str)
    {
        /* TODO : Internationnalization for the phone Number */
        $number = preg_replace('/\s+/', '', $str);
        if (is_numeric($number)) {
            return substr($number, -9);
        } else {
            return $str;
        }
    }

    /**
     * @param int $length
     * @return string
     */
    public static function random($length = 16)
    {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //length:36
        $final_rand = '';
        for ($i = 0; $i < $length; $i++) {
            $final_rand .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $final_rand;
    }
}
