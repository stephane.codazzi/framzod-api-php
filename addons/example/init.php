<?php

namespace framzod\addons;

use framzod\core\Framaddons;

/**
 * Class Example
 * Defines to use : (lines need to be copied in config.php)
 * define(EXEMPLE_ADDON_AWERSOME, "%s is a good developper !");
 * Defines addons need to have this format : [addon name in uppercase]_ADDON_[var name in uppercase]
 * @package framzod\addons
 */
class Example extends Framaddons
{
    /**
     * @var string
     */
    public $name = 'Example';
    /**
     * @var string
     */
    public $author = 'Stéphane Codazzi';
    /**
     * @var string
     */
    public $version = '1.0';
    /**
     * @var string
     */
    public $website = 'https://codazzi.fr';
    /**
     * @var string
     */
    public $description = "Example is not a functionnal addon: it's just for have a skelleton for create new addon";
    /**
     * @var string
     */
    public $licence = 'MIT';

    /**
     * Example constructor.
     */
    public function __construct()
    {
    }

    /**
     *
     */
    public function init()
    {
    }

    /**
     *
     */
    public function __destruct()
    {
    }

    /**
     * @return string
     */
    public function myAwersomeFunction()
    {
        return sprintf(EXEMPLE_ADDON_AWERSOME, $this->author);
    }
}
