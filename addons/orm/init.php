<?php

namespace framzod\addons;

use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Cache\RedisCache;
use framzod\core\Framaddons;

/*
*       Doctrine requirement change to vendors lib (via composer)
*       Defines to use : (lines need to be copied in config.php)
*
*/

/**
 * Class Orm
 * @package framzod\addons
 */
class Orm extends Framaddons
{
    /**
     * @var string
     */
    public $name = 'Orm';
    /**
     * @var string
     */
    public $author = 'CodazziS';
    /**
     * @var string
     */
    public $version = '1.1';
    /**
     * @var string
     */
    public $website = '';
    /**
     * @var string
     */
    public $description = "Orm can only be used by doctrine for now";
    /**
     * @var string
     */
    public $licence = '';

    /**
     * @var null
     */
    public $em = null;

    /**
     * Orm constructor.
     */
    public function __construct()
    {
        include_once __DIR__ . '/../../vendor/autoload.php';
        if (ORM_TYPE == "doctrine") {
            $this->initDoctrine();
        } else {
            die("ORM : ORM_TYPE have an unkown value.");
        }
    }

    /**
     *
     */
    public function __destruct()
    {
    }

    /**
     *
     */
    public function initDoctrine()
    {
        $paths = array("src/entities");
        $isDevMode = (SITEVERSION === "dev");// Need to fix the cache
        // the connection configuration
        $dbParams = array(
            'driver' => ORM_DRIVER,
            'user' => ORM_DB_USER,
            'password' => ORM_DB_PASSWORD,
            'dbname' => ORM_DB_NAME,
            'host' => ORM_DB_HOST,
            'charset' => ORM_DB_CHARSET
        );

        $redis = new \Redis();
        $redis->connect(ORM_DB_REDIS_HOST, ORM_DB_REDIS_PORT);

        $cacheDriver = new RedisCache();
        $cacheDriver->setRedis($redis);
        $cacheDriver->setNamespace(ORM_DB_REDIS_NAMESPACE);

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, $cacheDriver);
        try {
            $this->em = EntityManager::create($dbParams, $config);
        } catch (ORMException $e) {
            var_dump("DOCTRINE ERROR");
        }
    }
}
