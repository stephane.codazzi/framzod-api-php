<?php
// bootstrap.php
include_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . "/../../config/config.php";

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Cache\RedisCache;

$paths = array("src/entities");
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'driver' => ORM_DRIVER,
    'user' => ORM_DB_USER,
    'password' => ORM_DB_PASSWORD,
    'dbname' => ORM_DB_NAME,
    'host' => ORM_DB_HOST
);


$redis = new \Redis();
$redis->connect(ORM_DB_REDIS_HOST, ORM_DB_REDIS_PORT);

$cacheDriver = new RedisCache();
$cacheDriver->setRedis($redis);
$cacheDriver->setNamespace(ORM_DB_REDIS_NAMESPACE);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, $cacheDriver);
try {
    $entityManager = EntityManager::create($dbParams, $config);
} catch (\Doctrine\ORM\ORMException $e) {
    var_dump("DOCTRINE ERROR");
}
