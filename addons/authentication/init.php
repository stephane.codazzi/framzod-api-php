<?php

namespace framzod\addons;

use framzod\core\Framaddons;

/*
*
*/

/**
 * Class Authentication
 * @package framzod\addons
 */
class Authentication extends Framaddons
{
    /**
     * @var string
     */
    public $name = 'authentication';
    /**
     * @var string
     */
    public $author = 'Stéphane Codazzi';
    /**
     * @var string
     */
    public $version = '1.0';
    /**
     * @var string
     */
    public $website = 'https://codazzi.fr';
    /**
     * @var string
     */
    public $description = "Method for authentication, with API call";
    /**
     * @var string
     */
    public $licence = 'MIT';

    /**
     * @var bool
     */
    private $auth = false;

    /**
     * Authentication constructor.
     */
    public function __construct()
    {
        date_default_timezone_set('Europe/Paris');
        if (isset($_COOKIE['token']) && $_COOKIE['token'] != null) {
            $this->auth = true;
        }
    }

    /**
     * @return bool : current authentication status of the user
     */
    public function isAuth()
    {
        return $this->auth;
    }

    /**
     *
     */
    public function logout()
    {
        setcookie("key", "", time() - 1, '/');
        setcookie("token", "", time() - 1, '/');
        setcookie("email", "", time() - 1, '/');
        setcookie("user", "", time() - 1, '/');
        $this->auth = false;
    }

    /**
     * Call the API for authentified the user.
     * @param $parent
     * @param $email
     * @param $password
     * @return bool
     */
    public function login($parent, $email, $password)
    {
        $res = Apy::call(
            $parent,
            'Users',
            'GetToken',
            'GET',
            array('email' => $email, 'password' => $password, 'type' => 'Web')
        );
        if (isset($res['error']) && $res['error'] === 0) {
            setcookie("key", $res['key'], time() + 86400, '/');
            setcookie("token", $res['token'], time() + 86400, '/');
            setcookie("email", $email, time() + 86400, '/');
            setcookie("user", $res['user'], time() + 86400, '/');
            $this->auth = true;
        }
        return $this->auth;
    }

    /**
     * Call the API for create an user
     * @param $parent
     * @param $email
     * @param $password
     * @return mixed true, or error code(CF API)
     */
    public function signin($parent, $email, $password)
    {
        return Apy::call($parent, 'Users', 'Create', 'POST', array('email' => $email, 'password' => $password));
    }
}
