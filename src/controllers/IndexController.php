<?php

namespace framzod\controllers;

use framzod\addons\apy;
use entities\Visit;

class IndexController extends DefaultController
{
    public function __construct()
    {
        $this->render_class = 'html';
        $this->title        = 'Home Website';
        $this->view         = "index.html";
        $this->result['layout'] = "pages/home.html";
    }

    public function index()
    {
        $res1 = Apy::call($this, 'Visit', 'add', 'POST', array());
        $res2 = Apy::call($this, 'Visit', 'count', 'GET', array());

        if ($res1['error'] == 0 && $res2['error'] == 0) {
            $this->result['controller_result'] =
                "Hello !, your are the " . $res2['visits']."th visit";
        } else {
            $this->result['controller_result'] =
                "Hello !, you need to configure Doctrine for database connexion.";
        }
    }
}
