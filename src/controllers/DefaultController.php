<?php

namespace framzod\controllers;

use framzod\core\FzController;
use framzod\addons\apy;

class DefaultController extends FzController
{
    public $user = null;

    public function __construct()
    {
        $this->render_class = 'html';
        $this->view = "index.html";
        $this->result['title'] = 'Page introuvable';
        $this->result['layout'] = "pages/error_404.html";
    }

    public function init()
    {
        return true;
    }
}
