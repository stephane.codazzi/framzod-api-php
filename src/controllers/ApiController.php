<?php
namespace framzod\controllers;

use framzod\core\FzController;

class ApiController extends FzController
{
    public function __construct()
    {
        $this->render_class = 'html';
        $this->title        = 'Home API';
        $this->view         = "api/index.html";
    }

    public function __call($class, $args)
    {
        $start_time = time();

        if (!empty($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
            header('Access-Control-Allow-Headers: ' . $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
        }

        if (!class_exists('Api'.ucfirst($class))) {
            $class_file = 'api/' . ucfirst($class) . 'Controller.php';
            include_once $class_file;
        }
        $api_class_name = 'framzod\controllers\Api' . ucfirst($class) . 'Controller';
        $api_class = new $api_class_name();

        /* INIT */
        $api_class->request = $this->request;
        $api_class->data = $this->data;
        $api_class->addons = $this->addons;
        $api_class->em = $this->em;
        $api_class->init();

        /* CALL */
        if (isset($this->fragments[2])) {
            $api_class->{$this->fragments[2]}();
        } else {
            $api_class->index();
        }

        $this->result               = $api_class->result;
        $this->result['error']      = $api_class->error;
        $this->result['exec_time']  = time() - $start_time;
        $this->render_class         = $api_class->render_class;
        $this->title                = $api_class->title;
        $this->view                 = $api_class->view;
    }
}
