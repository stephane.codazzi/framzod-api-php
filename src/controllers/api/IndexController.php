<?php
namespace framzod\controllers;

use framzod\controllers\DefaultController;
use framzod\addons\apy;

/**
 * Index Controller
 */
class ApiIndexController extends ApiDefaultController
{
    public function __construct()
    {
        $this->render_class = 'json';
        $this->title        = 'Home API';
        $this->view         = "api/index.html";
    }

    public function tester()
    {
        $this->result['data'] = $this->data;
        $this->result['request'] = $this->request;
    }

    public function index()
    {
        $this->render_class = 'html';
        $this->result = array('lines' => array());

        $this->result['lines'][] = '<a href="/Api/Error">Errors</a> : Errors codes list.';
        $this->result['lines'][] = '<a href="/Api/Visit">Visit</a> : Visits management.';
    }
}
