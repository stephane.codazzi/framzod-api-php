<?php

namespace framzod\controllers\tests\units;

use framzod\core\Framzod;
use atoum;

/**
 * @engine inline
 */
class ApiVisitController extends atoum
{
    private $testedClass;

    public function __construct()
    {
        include_once __DIR__ . '/../../../../../config/config.php';
        include_once CORE_PATH . '/framzod.php';
        $framzod = new Framzod(false);
        $this->testedClass = $framzod->prepareclass("api/Visit", 'ApiVisit');
        require_once __DIR__ . '/../../IndexController.php';
        parent::__construct();
    }

    public function testCount()
    {
        $lastCount = 0;
        /* Bad method */
        $this->testedClass->request->method      = "POST";
        $this->testedClass->count();
        $this->integer($this->testedClass->error)->isEqualTo(1);

        /* Good method */
        $this->testedClass->request->method      = "GET";
        $this->testedClass->count();
        $this->integer($this->testedClass->error)->isEqualTo(0);
        $this->integer($this->testedClass->result["visits"])->isGreaterThanOrEqualTo(0);
        $lastcount = $this->testedClass->result["visits"];

        /* Add one visit */
        $this->testedClass->request->method      = "POST";
        $this->testedClass->add();

        /* Good method */
        $this->testedClass->request->method      = "GET";
        $this->testedClass->count();
        $this->integer($this->testedClass->error)->isEqualTo(0);
        $this->integer($this->testedClass->result["visits"])->isEqualTo($lastcount + 1);
        $lastcount = $this->testedClass->result["visits"];
    }

    public function testadd()
    {
        /* Bad method */
        $this->testedClass->request->method      = "GET";
        $this->testedClass->add();
        $this->integer($this->testedClass->error)->isEqualTo(1);

        /* Good method */
        $this->testedClass->request->method      = "POST";
        $this->testedClass->add();
        $this->integer($this->testedClass->error)->isEqualTo(0);
    }
}
