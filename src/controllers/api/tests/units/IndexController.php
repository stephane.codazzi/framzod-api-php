<?php

namespace framzod\controllers\tests\units;

use framzod\core\Framzod;
use atoum;

/**
 * @engine inline
 */
class ApiIndexController extends atoum
{
    private $testedClass;

    public function __construct()
    {
        include_once __DIR__ . '/../../../../../config/config.php';
        include_once CORE_PATH . '/framzod.php';
        $framzod = new Framzod(false);
        $this->testedClass = $framzod->prepareclass("api/Index", 'ApiIndex');
        require_once __DIR__ . '/../../IndexController.php';
        parent::__construct();
    }

    public function testTester()
    {
        /* Test the tester */
        $this->testedClass->request->method = "POST";
        $this->testedClass->data['test_data'] = 'test_value';
        $this->testedClass->tester();

        $this->integer($this->testedClass->error)->isEqualTo(0);
        $this->string($this->testedClass->result["request"]->method)->isEqualTo('POST');
    }
}
