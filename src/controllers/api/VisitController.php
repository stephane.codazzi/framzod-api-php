<?php
namespace framzod\controllers;

//require_once ENTITY_PATH . '/Visit.php';

use framzod\core\FzController;
use framzod\addons\apy;
use entities\Visit;

/**
 * Visit Controller
 */
class ApiVisitController extends ApiDefaultController
{
    public function __construct()
    {
        $this->render_class = 'json';
        $this->title        = 'Visits';
        $this->view         = "api/doc.html";
    }
    
    /**
     * @method POST
     * @name add
     * @description Add a new visit
     * @return $error (int) : Error code. Details in /api/error
     */
    public function add()
    {
        $this->error = -1;
        $conditions = array(
            'method'    => 'POST',
            'external'  => null,
            'require'   => array()
        );
        if ($this->addons['Apy']->isValid($this, $conditions)) {
            $visit = new Visit();
            $visit->setVisitedAt(new \DateTime());
            $this->em->persist($visit);
            $this->em->flush();

            $this->error = 0;
        }
    }
    
    /**
     * @method GET
     * @name count
     * @description count numbers of visits
     * @return $visits (int) : Visits counter
     * @return $error (int) : Error code. Details in /api/error
     */
    public function count()
    {
        $this->error = -1;
         $conditions = array(
            'method'    => 'GET',
            'external'  => null,
            'require'   => array()
        );
        if ($this->addons['Apy']->isValid($this, $conditions)) {
            $repository = $this->em->getRepository('entities\Visit');
            $this->result['visits'] = intval($repository->count());
            $this->error = 0;
        }
    }
    
    public function index()
    {
        $this->render_class = 'html';
        $this->result = array('name' => $this->title, 'docs' => array());
        
        $reflector = new \ReflectionClass(get_class($this));
        $this->result['class_description'] = $this->parseClass($reflector->getDocComment());
        $class_methods = get_class_methods(get_class($this));
        foreach ($class_methods as $method_name) {
            $doc = $reflector->getMethod($method_name)->getDocComment();
            if ($doc != null) {
                $this->result['docs'][] = $this->parseDoc($doc);
            }
        }
    }
}
