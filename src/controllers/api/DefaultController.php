<?php

namespace framzod\controllers;

use framzod\core\FzController;

/**
 * Default Controller
 */
class ApiDefaultController extends FzController
{
    protected $current_user = null;

    public function __construct()
    {
        parent::__construct();
        $this->render_class = 'json';
        $this->view = "api/doc.html";
    }

    public function init()
    {
        $this->authUser();
    }

    private function authUser()
    {
        if (!empty($this->data['email']) && !empty($this->data['token'])) {
            $userRepository = $this->em->getRepository('entities\User');
            $tokenRepository = $this->em->getRepository('entities\Token');
            $user = $userRepository->findByEmail($this->data['email']);
            $token = $tokenRepository->findToken($this->data['token'], $user);
            if (!empty($token)) {
                $this->current_user = $user;
            }
        }
    }

    public function index()
    {
        $this->render_class = 'html';
        $this->result = array('name' => $this->title, 'docs' => array());

        $reflector = new \ReflectionClass(get_class($this));
        $this->result['class_description'] = $this->parseClass($reflector->getDocComment());
        $classMethods = get_class_methods(get_class($this));
        foreach ($classMethods as $methodName) {
            $doc = $reflector->getMethod($methodName)->getDocComment();
            if ($doc != null) {
                $this->result['docs'][] = $this->parseDoc($doc);
            }
        }
    }
}
