<?php

namespace framzod\commands;

class SitemapCommand
{
    public $em = null;

    public function run()
    {
        include ADDON_PATH . '/sitemap/Sitemap.php';
        $sitemap = new \Sitemap(SITE_URL);
        $sitemap->setPath(ROOT_PATH . '/web/map/');
        $sitemap->addItem('/', '1.0', 'daily', 'Today');
        $sitemap->createSitemapIndex(SITE_URL . '/map/', 'Today');
    }
}
