<?php
namespace entities;

use Doctrine\ORM\Mapping\Index;

/**
 * @Entity @Table(name="visits")
 * @Entity @Entity(repositoryClass="entities\VisitRepository")
 **/

class Visit
{
    /**
 * @Id @Column(type="integer") @GeneratedValue
*/
    private $id;

    /**
 * @Column(type="datetime")
*/
    private $visited_at;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set visited_at
     *
     * @param  \DateTime $visitedAt
     * @return Visit
     */
    public function setVisitedAt($visitedAt)
    {
        $this->visited_at = $visitedAt;

        return $this;
    }

    /**
     * Get visited_at
     *
     * @return \DateTime
     */
    public function getVisitedAt()
    {
        return $this->visited_at;
    }
}
