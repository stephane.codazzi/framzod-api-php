<?php

namespace entities;

use Doctrine\ORM\EntityRepository;

class VisitRepository extends EntityRepository
{
    public function count()
    {
        $qb = $this->createQueryBuilder('i')
            ->select('COUNT(i)');
        return $qb
            ->getQuery()
            ->getSingleScalarResult();
    }
}
