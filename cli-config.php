<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once "addons/orm/bootstrap.php";
return ConsoleRunner::createHelperSet($entityManager);
