<?php

namespace framzod\core;

use framzod\core\FzController;
use framzod\core\FzRequest;
use framzod\core\Framaddons;

/**
 * Class Framzod
 * @package framzod\core
 */
class Framzod
{
    /**
     * @var \framzod\core\FzRequest|null
     */
    public $fz_request = null;
    /**
     * @var array
     */
    public $addons = array();
    /**
     * @var null
     */
    protected $fragments = null;
    /**
     * @var null
     */
    protected $lang = null;

    /**
     * Framzod constructor.
     * @param bool $cron
     */
    public function __construct($cron = false)
    {
        $this->setDebugConfig();

        if (!$cron) {
            ini_set("url_rewriter.tags", "");

            // Class
            include_once 'class/framaddons.class.php';
            include_once 'class/request.class.php';
            include_once 'class/controller.class.php';

            $this->parseUrl();
            $this->fz_request = new FzRequest();
            $this->fz_request->url = (!empty($_SERVER['HTTP_REQUEST'])) ? $_SERVER['HTTP_REQUEST'] : null;
            $this->fz_request->path = getcwd();
            $this->fz_request->class = (!empty($this->fragments[0])) ? ucwords($this->fragments[0]) : 'Index';
            $this->fz_request->method_name = (!empty($this->fragments[1])) ? ucwords($this->fragments[1]) : 'Index';
        }
        $this->autoInclude();
        $this->loadAddons();
    }

    /**
     *
     */
    private function setDebugConfig()
    {
        /*
        *   Check the version of the website
        *   and show or not the log in html
        *   Reset OPCACHE if site is not in production
        */
        if (SITEVERSION == 'dev') {
            opcache_reset();
            error_reporting(E_ALL);
            ini_set("display_errors", 1);
        } else {
            ini_set("display_errors", 0);
            error_reporting(0);
        }
    }

    /**
     *
     */
    private function autoInclude()
    {
        // auto include
        foreach ($GLOBALS['AUTO_INCLUDE'] as $file) {
            if (file_exists($file)) {
                include_once $file;
            } else {
                die('File ' . $file . ' not found.');
            }
        }
    }

    /**
     *
     */
    private function loadAddons()
    {
        // Include all addons
        foreach ($GLOBALS['ADDONS_ENABLE'] as $addon) {
            $addon_file = ADDON_PATH . '/' . $addon . '/init.php';
            if (file_exists($addon_file)) {
                include_once $addon_file;
                $addon_class = ucwords($addon);
                $class_path = 'framzod\addons\\' . $addon_class;
                $this->addons[$addon_class] = new $class_path();
            } else {
                die('Addon ' . $addon . ' not found.');
            }
        }
    }

    /**
     *
     */
    private function parseUrl()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            if (strpos($_SERVER['REQUEST_URI'], '?') !== false) {
                $uri = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?'));
            } else {
                $uri = $_SERVER['REQUEST_URI'];
            }
            $fragments = explode('/', $uri);
            /* Remove first empty filed */
            if (empty($fragments[0])) {
                $fragments = array_splice($fragments, 1);
            }
            /* Get the lang and remove it from fragments */
            if (isset($fragments[0]) && strlen($fragments[0]) == 2) {
                $this->lang = $fragments[0];
                $fragments = array_splice($fragments, 1);
            }
            $this->fragments = $fragments;
        }
    }

    /**
     *
     */
    public function main()
    {
        if (file_exists(SOURCES_PATH . '/controllers/' . $this->fz_request->class . 'Controller.php')) {
            include_once SOURCES_PATH . '/controllers/' . $this->fz_request->class . 'Controller.php';
        } else {
            $this->fz_request->class = ERROR404_CLASSFILE;
            $this->fz_request->method_name = ERROR404_METHOD;
            include_once SOURCES_PATH . '/controllers/' . $this->fz_request->class . 'Controller.php';
        }
        /* Création de la classe du site */
        $className = 'framzod\controllers\\' . $this->fz_request->class . 'Controller';

        if (!method_exists($className, $this->fz_request->method_name)
            && !in_array($this->fz_request->class, $GLOBALS['ROUTER_CLASS'])) {
            $this->fz_request->class = ERROR404_CLASSFILE;
            $this->fz_request->method_name = ERROR404_METHOD;
            include_once SOURCES_PATH . '/controllers/' . $this->fz_request->class . 'Controller.php';
            $className = 'framzod\controllers\\' . $this->fz_request->class . 'Controller';
        }
        $this->loadMethod($className);
    }

    /**
     * @param $className
     */
    private function loadMethod($className)
    {
        /* Create Request class */
        $fz_class = new $className();
        $fz_class->request = $this->fz_request;
        $fz_class->data = $fz_class->request->data;
        $fz_class->fragments = $this->fragments;
        $fz_class->addons = $this->addons;
        if (isset($this->addons['Orm'])) {
            $fz_class->em = $this->addons['Orm']->em;
        }
        if (isset($this->addons['Lang'])) {
            $fz_class->lang = $this->addons['Lang']->getLang();
        }
        if (method_exists($className, 'init')) {
            $fz_class->init();
        }
        $fz_class->{$this->fz_request->method_name}();
        if ($fz_class->render_class == 'html') {
            $fz_class->result['lang'] = $fz_class->lang;
            $this->addons[ucfirst($fz_class->render_class)]->render($fz_class);
        } elseif ($fz_class->render_class == 'text') {
            echo $fz_class->result['text'];
        } elseif ($fz_class->render_class == 'json') {
            echo json_encode($fz_class->result);
        }
    }

    /**
     * @param $args
     */
    public function launchCommand($args)
    {
        $className = 'framzod\commands\\' . $args[1] . 'Command';
        /* Create Request class */
        $fz_class = new $className();
        $fz_class->request = $this->fz_request;
        $fz_class->data = $args;
        $fz_class->fragments = $this->fragments;
        $fz_class->addons = $this->addons;
        if (isset($this->addons['Orm'])) {
            $fz_class->em = $this->addons['Orm']->em;
        }
        if (isset($this->addons['Lang'])) {
            $fz_class->lang = $this->addons['Lang']->getLang();
        }
        $fz_class->run();
    }

    /**
     * @param $file
     * @param $class
     * @return mixed
     */
    public function prepareclass($file, $class)
    {
        if (file_exists(SOURCES_PATH . '/controllers/' . $file . 'Controller.php')) {
            include_once SOURCES_PATH . '/controllers/' . $file . 'Controller.php';
        } else {
            die("Class not found");
        }
        /* Création de la classe du site */
        $className = 'framzod\controllers\\' . $class . 'Controller';

        /* Create Request class */
        $fz_class = new $className();
        $fz_class->request = $this->fz_request;
        $fz_class->data = $fz_class->request->data;
        $fz_class->fragments = $this->fragments;
        $fz_class->addons = $this->addons;
        if (isset($this->addons['Orm'])) {
            $fz_class->em = $this->addons['Orm']->em;
        }
        if (isset($this->addons['Lang'])) {
            $fz_class->lang = $this->addons['Lang']->getLang();
        }
        if (method_exists($className, 'init')) {
            $fz_class->init();
        }

        return $fz_class;
    }
}
