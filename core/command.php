<?php

use framzod\core\Framzod;

if (empty($argv) || empty($argv[1])) {
    echo "You need to specify command name.\n";
    exit(-1);
} elseif (!file_exists(__DIR__ . '/../config/config.php')) {
    echo "Config file not found\n";
} elseif (!file_exists(__DIR__ . '/../src/commands/' . $argv[1] . 'Command.php')) {
    echo "Command file not found\n";
} else {
    include_once __DIR__ . '/../config/config.php';
    include_once CORE_PATH . '/framzod.php';
    include_once __DIR__ . '/../src/commands/' . $argv[1] . 'Command.php';
    $framzod = new Framzod();
    $framzod->launchCommand($argv);
    exit(0);
}
exit(-1);
