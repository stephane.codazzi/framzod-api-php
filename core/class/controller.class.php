<?php

namespace framzod\core;

/**
 * Class FzController
 * @package framzod\core
 */
/**
 * Class FzController
 * @package framzod\core
 */
class FzController
{
    /**
     * @var null
     */
    public $render_class = null;
    /**
     * @var string
     */
    public $view = "index.html";
    /**
     * @var null
     */
    public $user_id = null; // User_id when user the authentication class
    /**
     * @var FzRequest
     */
    public $request = null;
    /**
     * @var null
     */
    public $data = null;
    /**
     * @var null
     */
    public $addons = null;
    /**
     * @var null
     */
    public $em = null;

    /**
     * @var array
     */
    public $result = array();
    /**
     * @var int
     */
    public $error = 0;
    /**
     * @var string
     */
    public $title = "";

    /**
     *
     */
    public function __destruct()
    {
    }

    /**
     * FzController constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $method
     * @param $requirements
     * @param $authentication
     * @return bool
     */
    public function start($method, $requirements, $authentication)
    {
        if ($method != $this->request->method) {
            $this->error = 1;
            return false;
        }

        if ($requirements != null) {
            foreach ($requirements as $var) {
                if (!isset($this->request->data[$var])) {
                    $this->error = 2;
                    return false;
                }
            }
        }

        if ($authentication) {
            $auth = false;
            if (isset($this->datas['username'])
                && isset($this->datas['token'])
            ) {
                $auth = $this->addons['Authentication']->is_auth($this->data['username'], $this->data['token']);
            }
            if (!$auth) {
                $this->error = 3;
                return false;
            }
        }
        return true;
    }

    /**
     *
     */
    public function putResult()
    {
        if ($this->render_class === 'json') {
            $this->render_class['error'] = $this->error;
            header('Content-Type: application/json');
            echo json_encode($this->render_class);
        } elseif ($this->render_class == 'html') {
            Designer::put_result_html($this->title, $this->render_class);
        }
    }

    /**
     * @return array
     */
    public function getResult()
    {
        $this->result['error'] = $this->error;
        return $this->result;
    }

    /**
     * @param $other_inst
     */
    public function copyAttrs($other_inst)
    {
        $this->request = $other_inst->request;
        $this->data = $other_inst->data;
        $this->addons = $other_inst->addons;
        $this->lang = $other_inst->lang;
    }

    /**
     * @param $class_doc
     * @return string
     */
    protected function parseClass($class_doc)
    {
        $class_doc = str_replace('/**', '', $class_doc);
        $class_doc = str_replace('*/', '', $class_doc);
        $class_doc = str_replace(' *', '', $class_doc);
        $split = explode('<br />', nl2br($class_doc));
        $return = '';
        foreach ($split as $line) {
            $return .= $line;
        }
        return $return . "\n";
    }

    /**
     * @param $class_doc
     * @return array
     */
    protected function parseDoc($class_doc)
    {
        $class_doc = str_replace('/**', '', $class_doc);
        $class_doc = str_replace('*/', '', $class_doc);
        $class_doc = str_replace(' *', '', $class_doc);
        $split = explode('<br />', nl2br($class_doc));
        $return = array(
            'method' => '',
            'description' => '',
            'params' => array(),
            'returns' => array()
        );

        foreach ($split as $line) {
            if (strpos($line, '@method')) {
                $return['method'] = trim(str_replace('@method ', '', $line));
            } elseif (strpos($line, '@name')) {
                $return['name'] = str_replace('@name ', '', $line);
            } elseif (strpos($line, '@description')) {
                $return['description'] = str_replace('@description ', '', $line);
            } elseif (strpos($line, '@param')) {
                $return['params'][] = str_replace('@param ', '', $line);
            } elseif (strpos($line, '@return')) {
                $return['returns'][] = str_replace('@return ', '', $line);
            }
        }
        return $return;
    }
}
