<?php

namespace framzod\core;

/**
 * Class FzRequest
 * @package framzod\core
 */
class FzRequest
{
    /**
     * @var string
     */
    public $method = '';
    /**
     * @var array
     */
    public $data = [];
    /**
     * @var string
     */
    public $url = '';
    /**
     * @var string
     */
    public $path = '';
    /**
     * @var string
     */
    public $fragments = '';
    /**
     * @var string
     */
    public $class = 'Index';
    /**
     * @var string
     */
    public $method_name = 'index';

    /**
     * FzRequest constructor.
     */
    public function __construct()
    {
        $this->method = (empty($_SERVER['REQUEST_METHOD']) ? 'GET' : $_SERVER['REQUEST_METHOD']);

        if ($this->method == 'GET') {
            $this->data = $_GET;
        } elseif ($this->method == 'PUT') {
            parse_str(file_get_contents("php://input"), $post_vars);
            $this->data = $post_vars;
        } else {
            $this->data = $_POST;
        }

        if (!isset($this->data['args'])) {
            $this->data['args'] = 'index';
        }
    }
}
