<?php

namespace framzod\core;

/**
 * Class Framaddons
 * Parent class for all addons
 * @package framzod\core
 */
class Framaddons
{

    /**
     * Framaddons constructor.
     */
    public function __construct()
    {
    }

    /**
     *
     */
    public function init()
    {
    }

    /**
     *
     */
    public function __destruct()
    {
    }

    /**
     * Function used only for the render class (see render addon)
     * @param $fzclass
     */
    public function render($fzclass)
    {
    }
}
